export default function Delete(db, table, id) {
  return new Promise((resolve, reject) => {
    try {
      let sql = `DELETE FROM ${table} WHERE id = ?;`;
      db.transaction(tx => {
        tx.executeSql(sql, [id], (_, result) => {
          console.log('delete result', result)
          resolve(result)
        })
      })
    } catch (error) {
      reject(error)
    }
  })
}
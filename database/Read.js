export default function Read(db, table, option = {}) {
    return new Promise((resolve, reject) => {
      try {
        let clauseOrder = '';
        let clauseWhere = '';
        let inputs = [];

        let oOrder = option.order;
        let oWhere = option.where;

        if (oOrder) clauseOrder = `ORDER BY ${oOrder.join(',')}`;
        if (oWhere && Object.keys(oWhere).length) {
          var keys = Object.keys(oWhere);
          keys.forEach(e => {
            if (!oWhere[e].exp || !oWhere[e].value) return;
            if (oWhere[e].exp == 'in') {
              let values = oWhere[e].value;
              values.forEach(val => {
                inputs.push(val);
              })
              clauseWhere += ` AND ${e} in (${values.map(e => '?').join(',')}) `;
              return
            }
            inputs.push(oWhere[e].value);
            clauseWhere += ` AND ${e} ${oWhere[e].exp} ?`;
          })
        }

        let sql = `SELECT * FROM ${table} WHERE 1=1 ${clauseWhere} ${clauseOrder};`
        db.transaction(tx => {
          tx.executeSql(sql, inputs, (_, { rows }) => {
            let data = rows._array;
            resolve({data});
          })
        })
      } catch (error) {
        console.log(error);
        reject({error});
      }
    })
}
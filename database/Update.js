export default function Update(db, table, data) {
  return new Promise((resolve, reject) => {
    try {
      let prop = Object.keys(data);

      let values = [];
      let inputs = [];
      
      prop.forEach(e => {
        if (e == 'id') return;
        values.push(` ${e} = ? `);
        inputs.push(data[e]);
      })

      let sql = `UPDATE ${table} SET ${values.join(',')} WHERE id = ?;`
      inputs.push(data['id']);

      db.transaction(tx => {
        tx.executeSql(sql, inputs, (_, result) => {
          console.log('update result', result);
          resolve(result);
        }, (_, error) => {
          console.log('update error', error);
          reject(error);
        })
      })
    } catch (error) {
      console.log(error);
      reject({error});
    }
  })
}
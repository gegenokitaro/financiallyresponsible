export default function Init(db, test = false) {
  let dropItems = `DROP TABLE IF EXISTS items;`
  let dropCategories = `DROP TABLE IF EXISTS categories`;

  let initItems = `CREATE TABLE IF NOT EXISTS items (id integer primary key autoincrement not null, isincome integer not null default 0, date datetime default current_timestamp, category_id integer, price real, description text);`;
  let initCategories = `CREATE TABLE IF NOT EXISTS categories (id integer primary key not null, name text, isincome integer not null default 0);`
  let initWishlist = `CREATE TABLE IF NOT EXISTS wishlist (id integer primary key not null, name text, price real, group_id integer default 0, item_priority integer default 0);`
  let initWishlistGroup = `CREATE TABLE IF NOT EXISTS wishlistgroup (id integer primary key not null, name text, group_priority integer 0);`
  let initWishlistGroupSubstract = `CREATE TABLE IF NOT EXISTS wishlistgroupsubstract (id integer primary key not null, group_id integer, category_id integer);`

  let checkCategories = `SELECT * FROM categories`;
  let defaultCategories = `INSERT INTO categories (name, isincome) VALUES ('Belanja', 0), ('Tagihan', 0), ('Gaji', 1);`
  try {
    db.transaction((tx) => {
      if (test) {
        tx.executeSql(dropItems);
        tx.executeSql(dropCategories);
      }
      tx.executeSql(initItems);
      tx.executeSql(initCategories);
      tx.executeSql(initWishlistGroup);
      tx.executeSql(initWishlistGroupSubstract);
      tx.executeSql(initWishlist);
      tx.executeSql(checkCategories, [], (_, result) => {
        let dataCategories = result.rows._array;
        if (dataCategories.length == 0) {
          tx.executeSql(defaultCategories);
        }
      })
    });
  } catch (error) {
    console.log(error)
  }
}
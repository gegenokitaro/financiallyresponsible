export default function Create(db, table, data) {
  return new Promise((resolve, reject) => {
    try {
      const keys = Object.keys(data);
      const columns = keys.join(', ');
      const values = keys.map(() => '?').join(',');
      const inputs = Object.values(data);
  
      let sql = `INSERT INTO ${table} (${columns}) VALUES (${values})`;
      db.transaction(tx => {
        tx.executeSql(sql, inputs, (_, result) => {
          console.log('create result', result)
          resolve(result);
        }, (_, error) => {
          console.log('create error', error)
          reject(error);
        });
      })
    } catch (error) {
      reject(error)
    }
  })
}
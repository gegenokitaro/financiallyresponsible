import { StatusBar } from 'expo-status-bar';
import * as NavigationBar from 'expo-navigation-bar';
import { useEffect, useState } from 'react';
import { Platform, StyleSheet, TouchableOpacity, Text, View } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import ActivityModal from './components/ActivityModal';

import * as SQLite from 'expo-sqlite';
import Init from './database/Init';
import Create from './database/Create';
import ActivityHeader from './components/ActivityHeader';
import FilterComponent from './components/FilterComponent';
import Color from './styles/Color';
import ActivityItems from './components/ActivityItems';
import FormatDate from './utils/FormatDate';
import Delete from './database/Delete';
import Update from './database/Update';
import AnalyticItem from './components/AnalyticItem';
import WishlistItem from './components/WishlistItems';
import WishlistModal from './components/WishlistModal';

const test_ = false;

const backgroundColor = Color.colorGreen;
const borderColor = Color.colorGrey;
const buttonActiveColor = Color.colorBlue;
const buttonTextColor = Color.colorWhite;

export default function App() {
  const footerIcon = [
    {
      id: 0,
      name: "md-wallet-outline",
      onPress: () => handleMode(0)
    }, {
      id: 1,
      name: "md-receipt-outline",
      onPress: () => handleMode(1)
    }, {
      id: 2,
      name: "md-heart-outline",
      onPress: () => handleMode(2)
    }
  ]
  const db = SQLite.openDatabase('db.db');

  const [mode, setMode] = useState(0);

  const [saldo, setSaldo] = useState(0);
  const [income, setIncome] = useState(0);
  const [expense, setExpense] = useState(0);
  const [activityItem, setActivityItem] = useState();
  const [activityItems, setActivityItems] = useState([]);
  const [activityFilteredItems, setActivityFilteredItems] = useState([]);

  const [activityModalVisible, setActivityModalVisible] = useState(false);
  const [wishlistModalVisible, setWishlistModalVisible] = useState(false);

  useEffect(() => {
    NavigationBar.setVisibilityAsync("hidden");
    NavigationBar.setBehaviorAsync("overlay-swipe");
    
    if (Platform.OS === "android") {
      NavigationBar.setBackgroundColorAsync(backgroundColor);
    }

    console.log('useEffect app')
    
    Init(db, test_);
    LoadData();
  }, [])
  
  const showActivityModal = () => {
    setActivityModalVisible(true);
  }

  const hideActivityModal = () => {
    setActivityItem({});
    setActivityModalVisible(false);
  }

  const addActivityItem = async (item) => {
    let data = {
      isincome: item.isIncome,
      date: item.date.toISOString(),
      category_id: item.category.id,
      price: item.price,
      description: item.description === undefined ? "" : item.description 
    }

    await Create(db, "items", data);
    hideActivityModal();
    LoadData();
  }

  const delActivityItem = async (item) => {
    await Delete(db, "items", item.id);
    hideActivityModal()
    LoadData();
  }

  const updateActivityItem = async (item) => {
    await Update(db, "items", item);
    hideActivityModal();
    LoadData();
  }

  const pickActivityItem = (item) => {
    setActivityItem(item);
    showActivityModal();
  }

  const newActivityItem = () => {
    showActivityModal();
  }

  const showWishlistModal = () => {
    setWishlistModalVisible(true);
  }

  const hideWishlistModal = () => {
    setWishlistModalVisible(false);
  }

  const newWishlistItem = () => {
    showWishlistModal();
  }

  const newItem = () => {
    if (mode == 2) return newWishlistItem();
    else return newActivityItem();
  }

  const handleMode = (id) => {
    setMode(id);
  }

  const LoadData = async () => {
    try {
      const items = await ListItems();
      if (items.error) throw items.error 
      ComputeBalance(items.data)
      ComputeItems(items.data)
    } catch (error) {
      console.log(error)
    }
  }

  const ListItems = (filter = { whereclause: null, input: null }) => {
    return new Promise((resolve, reject) => {
      try {
        let whereclause = filter.whereclause ? filter.whereclause : '';
        let input = filter.input ? filter.input : [];
        let sql = `
        SELECT
          items.id,
          items.isincome,
          items.category_id,
          categories.name as category_name,
          items.date,
          items.price,
          items.description
        FROM items 
        LEFT JOIN categories 
          ON categories.id = items.category_id
        WHERE 1=1
        ${whereclause}
        ORDER BY items.date;`
        db.transaction(tx => {
          tx.executeSql(sql, input, (_, result) => {
            let data = result.rows._array;
            resolve({data})
          })
        })
      } catch (error) {
        console.log(error);
        reject({error})
      }
    })
  }

  const ComputeBalance = (data) => {
    function totalBasedIncome(data, isincome = 1) {
      return data.filter(e => e.isincome === isincome)
                    .reduce((acc, obj) => acc + obj.price, 0)
    }
    let income = totalBasedIncome(data, 1);
    let expense = totalBasedIncome(data, 0);
    let saldo = income - expense;

    setSaldo(saldo);
    setIncome(income);
    setExpense(expense);
  }

  const ComputeItems = (data) => {
    setActivityItems(data);
    setActivityFilteredItems(GroupingData(data));
  }

  const GroupingData = (data) => {
    return data.reduce((acc, value) => {
      let day = FormatDate(value['date']);
      if (!acc.length) {
        acc = [];
      }
      let index = acc.map(e => e.header).indexOf(day);
      let price = value.isincome == 1 ? value.price : value.price * -1;
      if (index > -1) {
        acc[index].data.push(value)
        acc[index].total += price
      } else {
        acc.push({
          header: day,
          total: price,
          data: [value]
        })
      }
      return acc;
    }, {})
  }

  const FilterItems = async (filter) => {
    let data = await ListItems(filter);
    setActivityFilteredItems(GroupingData(data.data));
  }

  const ListWishlist = (filter) => {
    return new Promise((resolve, reject) => {
      
    })
  }

  const renderSwitch = (id) => {
    switch (id) {
      case 0:
        return (<>
          <FilterComponent 
            db={db}
            onChangeFilter={FilterItems}
            />
          <ActivityItems 
            data={activityFilteredItems}
            activityItemHandler={pickActivityItem}
            />
        </>)
      case 1:
        return (<>
          <AnalyticItem db={db} mode={mode} />
        </>)
      default:
        return (<>
          <WishlistItem db={db} mode={mode} />
        </>)
    }
  }

  return (
    <View style={styles.container}>
      <ActivityHeader 
        saldo={saldo}
        income={income}
        expense={expense}
        />
      {renderSwitch(mode)}
      <ActivityModal 
        default={activityItem}
        visible={activityModalVisible}
        hideModal={hideActivityModal}
        onConfirm={addActivityItem}
        onDelete={delActivityItem}
        onUpdate={updateActivityItem}
        db={db}
        />
      <WishlistModal 
        visible={wishlistModalVisible}
        hideModal={hideWishlistModal}
        />
      <View style={[styles.footer]}>
        {footerIcon.map((icon) => {
          return (
            <FooterMenu key={icon.id} name={icon.name} onPress={icon.onPress} />
          )
        })}
      </View>
      <View style={styles.addActivityWrapper}>
        <TouchableOpacity 
          onPress={newItem}>
          <View style={[styles.addWrapper, mode == 2 ? {backgroundColor: Color.colorYellow} : '']}>
            <Ionicons 
              name='add-outline'
              size={32}
              color={Color.colorWhite}
              />
          </View>
        </TouchableOpacity>
      </View>
      <StatusBar style='light' hidden translucent backgroundColor='transparent' />
    </View>
  );
}

function FooterMenu (props) {
  let name = props.name;
  let onPress = props.onPress;

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.footerIcon]}>
        <Ionicons 
          name={name}
          size={40}
          color={Color.backgroundColor}
          />
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: Color.baseColor,
  },
  infoTitle: {

  },
  infoValue: {
    fontSize: 32,
    fontWeight: 'bold'
  },
  historyContainer: {
    paddingVertical: 0,
  },
  addActivityBase: {
    flex: 0
  },
  addActivityWrapper: {
    position: 'absolute',
    bottom: 15,
    right: 15,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  addWrapper: {
    height: 60,
    width: 60,
    borderRadius: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: borderColor,
    backgroundColor: buttonActiveColor
  },
  text: {
    color: Color.colorWhite
  },
  buttonText: {
    color: buttonTextColor,
  },
  footer: {
    height: 65,
    flexDirection: 'row',
    backgroundColor: Color.baseColor,
    padding: 5
  },
  footerIcon: {
    paddingVertical: 5,
    paddingHorizontal: 10
  }
});

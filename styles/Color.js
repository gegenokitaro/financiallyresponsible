const colorWhite = "#f9f7f3";
const colorRed = "#ef476f";
const colorBlue = "#118ab2";
const colorGreen = "#06d6a0";
const colorGrey = "#adb5bd";
const colorYellow = "#f97b22"

export default Color = {
  colorWhite,
  colorRed,
  colorBlue,
  colorGreen,
  colorGrey,
  colorYellow,
  baseColor: colorGreen,
  backgroundColor: colorWhite
}
const ArrayMath = {};

const sum = (array, prop) => {
  return array.reduce((acc, value) => {
    acc += value[prop];
    return acc
  }, 0)
}

ArrayMath.sum = sum;

export default ArrayMath;
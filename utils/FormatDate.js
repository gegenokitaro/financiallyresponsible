export default function FormatDate (date, separator = "/") {
  let targetDate = new Date(date);
  let dateYear = targetDate.getFullYear();
  let dateMonth = (targetDate.getMonth()+1).toString().padStart(2, '0');
  let dateDay = targetDate.getDate().toString().padStart(2, '0');

  let formattedDate = `${dateYear}${separator}${dateMonth}${separator}${dateDay}`
  return formattedDate
}
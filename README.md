### Financially Responsible
This is a small project to familiarize myself with react native (since the last time I put my hand on react's code, especially react native, was so long ago). This app's purpose was basically just one thing, to record financial activity.

The one who actually needs this app is my wife, so the feature(s) is at my wife requests.

#### To Do
- [x] Balance, income, expense (and data recording, basically base feature)
- [x] Filter data based on date and category
  -  [x] add filter for income/expense
- [x] CRUD item
  - [x] create item
  - [x] read item
  - [x] update item
  - [x] delete item
- [ ] Add new page for grouping and reporting
  - [x] Navigation (simple navigation that consists of 2 pseudo-page)
  - [ ] Wishlist target (my own feature)
  - [ ] Grouping based on date (day, month, year, week)
  - [ ] Reporting

#### How to

Install expo-cli globally, clone this project, npm install, expo start.

Phone/emulator required. Not built with web in mind.

Build with eas-cli 

`eas build -p android --profile test`

#### Ingredient

I use expo with react-native, some react-native modules for looks (check package.json for details), and use SQLite as database engine. I need a local, lightweight, SQL db engine because this app is completely offline and independent. The code is messy with lots of hack, but it works.

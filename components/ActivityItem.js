import { StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Color from "../styles/Color";

export default function ActivityItem(props) {
  let item = props.item

  const handleItem = (value) => {
		props.activityItemHandler(value);
  }

  return (
		<TouchableOpacity
			onPress={() => handleItem(item)}>
    	<View style={styles.itemContainer}>
      	<View style={[styles.item, styles.itemLeft]}>
        	<Text style={[styles.itemText]}>{item.category_name}</Text>
        </View>
        <View style={[styles.item, styles.itemCenter]}>
        	<Text style={[styles.itemText]}>{item.description}</Text>
        </View>
        <View style={[styles.item, styles.itemRight]}>
        	<Text style={[styles.itemText, styles.RightAlign, item.isincome ? styles.ColorBlue : styles.ColorRed]}>{item.price.toLocaleString('id-ID')}</Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
	itemContainer: {
		borderBottomWidth: 0.4,
		borderBottomColor: '#bbb',
		paddingHorizontal: 1,
		paddingVertical: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-start'
	},
	item: {
		height: '100%'
	},
	itemLeft: {
		// minWidth: 70,
		width: '20%'
	},
	itemCenter: {
		// minWidth: 70,
		width: '50%'
	},
	itemRight: {
		// minWidth: 90,
		width: '30%',
	},
	itemText: {
		fontSize: 13,
	},
	RightAlign: {
		textAlign: 'right'
	},
	ColorBlue: {
		color: Color.colorBlue
	},
	ColorRed: {
		color: Color.colorRed
	}
})
import { useEffect, useState } from "react";
import { View, Text, StyleSheet, Pressable } from "react-native";
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Ionicons from "@expo/vector-icons/Ionicons";
import Color from "../styles/Color";
import Read from "../database/Read";
import FormatDate from "../utils/FormatDate";

export default function FilterComponent(props) {
  const db = props.db;

  const [filterVisible, setFilterVisible] = useState(false)
  const [filterCategoryVisible, setFilterCategoryVisible] = useState(false);
  const [filterTypeVisible, setFilterTypeVisible] = useState(false);

  const [datePickerVisible, setDatePickerVisible] = useState(false);
  const [dateFromTo, setDateFromTo] = useState(0);

  const [whereDateFrom, setWhereDateFrom] = useState();
  const [whereDateTo, setWHereDateTo] = useState();
  const [whereCategory, setWhereCategory] = useState([]);
  const [whereType, setWhereType] = useState([]);

  const [listTypes, setListTypes] = useState([]);
  const [listTypesOption, setListTypesOption] = useState([]);
  const [listTypesPick, setListTypesPick] = useState([]);

  const [listCategories, setListCategories] = useState([]);
  const [listCategoriesOption, setListCategoriesOption] = useState([]);
  const [listCategoriesPick, setListCategoriesPick] = useState([]);

  useEffect(() => {
    let listPick = getCategoriesOption(listCategories, listCategoriesPick);
    setListCategoriesOption(listPick);
  }, [listCategoriesPick, listCategories])

  useEffect(() => {
    let listPick = getTypesOption(listTypes, listTypesPick);
    setListTypesOption(listPick);
    getCategories();
  }, [listTypesPick, listTypes])

  useEffect(() => {
    let whereclause = ' ';
    let input = [];
    if (whereDateFrom) {
      whereclause += ` AND date(date) >= date(?)`;
      input.push(FormatDate(whereDateFrom, '-'));
    }
    if (whereDateTo) {
      whereclause += ` AND date(date) <= date(?)`;
      input.push(FormatDate(whereDateTo, '-'));
    }
    if (whereCategory.length > 0) {
      whereclause += ` AND category_id IN (${whereCategory.map(() => "?").join(',')})`;
      input = [...input, ...whereCategory.map(e => e.id)];
    }

    if (whereType.length > 0) {
      whereclause += ` AND items.isincome IN (${whereType.map(() => "?").join(',')}) `;
      input = [...input, ...whereType.map(e => e.id)]
    }

    props.onChangeFilter({whereclause, input});
  }, [whereDateFrom, whereDateTo, whereCategory, whereType])

  useEffect(() => {
    setWhereCategory(listCategoriesPick);
  }, [listCategoriesPick])

  useEffect(() => {
    setWhereType(listTypesPick);
  }, [listTypesPick])

  const toggleFilter = () => {
    if (filterVisible) return hideFilter();
    showFilter();
  }

  const showFilter = () => {
    setFilterVisible(true)
  }

  const hideFilter = () => {
    setFilterVisible(false)
  }

  const toggleCategoryFilter = () => {
    if (filterCategoryVisible) return hideCategoryFilter();
    showCategoryFilter();
  }

  const showCategoryFilter = () => {
    getCategories();
    setFilterCategoryVisible(true);
  }

  const hideCategoryFilter = () => {
    setFilterCategoryVisible(false);
  }

  const toggleTypeFilter = () => { 
    getTypes();
    if (filterTypeVisible) return setFilterTypeVisible(false);
    setFilterTypeVisible(true);
  }

  const getCategories = async () => {
    let option = {
      order: ['isincome', 'name']
    }
    let categoriesOption = [];
    if (listTypesPick.length) {
      listTypesPick.map(e => e.isincome).forEach(e => {
        categoriesOption.push(e);
      })
    }
    if (categoriesOption.length) {
      option.where = {
        isincome: {
          exp: 'in',
          value: categoriesOption
        }
      }
    }
    let categories = await Read(db, 'categories', option);
    setListCategories(categories.data);
  }

  const getCategoriesOption = (target, compare) => {
    if (compare.length === 0) return target;

    return target.filter(cat => !compare.map(catP => catP?.id).includes(cat.id));
  }

  const handleCategoriesOption = (data) => {
    setListCategoriesPick([...listCategoriesPick, data]);
  }

  const handleCategoriesPick = (data) => {
    setListCategoriesPick(listCategoriesPick.filter(e => e.id !== data.id));
  }

  const getTypes = () => {
    let types = [
      { name: 'Pemasukan', isincome: 1, id: 1 },
      { name: 'Pengeluaran', isincome: 0, id: 0 },
    ]
    setListTypes(types);
  }

  const getTypesOption = (target, compare) => {
    if (compare.length === 0) return target;

    return target.filter(ty => !compare.map(tyP => tyP?.id).includes(ty.id));
  }

  const handleTypesOption = (data) => {
    setListTypesPick([...listTypesPick, data]);
  }

  const handleTypesPick = (data) => {
    setListTypesPick(listTypesPick.filter(e => e.id !== data.id));
  }

  const handleDatePicker = (value) => {
    hideDatePicker();
    if (dateFromTo == 0) return setWhereDateFrom(value);
    setWHereDateTo(value);
  }

  const handleDatePick = (state) => {
    if (state === 0) return setWhereDateFrom();
    setWHereDateTo();
  }

  const showDatePicker = (state) => {
    setDateFromTo(state);
    setDatePickerVisible(true);
  }

  const hideDatePicker = () => {
    setDatePickerVisible(false);
  }

  return (
    <View style={[style.filterContainer]}>
      <View style={[style.filterWidget]}>
        <Pressable
          onPress={toggleFilter}
          >
          <View style={[style.filterButton]}>
            <Ionicons 
              name={filterVisible ? "md-caret-down" : "md-caret-forward"}
              size={18}
              color={Color.colorWhite}
              />
              <Text style={[style.filterText, style.TextWhite]}>Filter</Text>
          </View>
        </Pressable>
      </View>
      {filterVisible ? (
      <View style={[style.filterWidgetInside]}>
        <View style={[style.filterWidget]}>
          <Pressable
            onPress={() => showDatePicker(0)}>
            <View style={[style.filterButton, style.FillGreen]}>
              <Text style={[style.TextWhite]}>From</Text>
            </View>
          </Pressable>
          {whereDateFrom ? (
            <CategoryButton 
              data={{name: FormatDate(whereDateFrom)}}
              onPress={() => handleDatePick(0)}
              isPick={true}
              />
          ) : (<></>)}
          <Pressable
            onPress={() => showDatePicker(1)}>
            <View style={[style.filterButton, style.FillGreen]}>
              <Text style={[style.TextWhite]}>To</Text>
            </View>
          </Pressable>
          {whereDateTo ? (
            <CategoryButton 
              data={{name: FormatDate(whereDateTo)}}
              onPress={() => handleDatePick(1)}
              isPick={true}
              />
          ) : (<></>)}
          <DateTimePickerModal 
            isVisible={datePickerVisible}
            onConfirm={handleDatePicker}
            onCancel={hideDatePicker}
            />
        </View>
        <View style={[style.filterWidget]}>
          <Pressable
            onPress={toggleTypeFilter}
            >
            <View style={[style.filterButton, style.FillGreen]}>
              <Ionicons 
                name={filterTypeVisible ? "md-caret-down" : "md-caret-forward"}
                size={18}
                color={Color.colorWhite}
                />
              <Text style={[style.filterText, style.TextWhite]}>Type</Text>
            </View>
          </Pressable>
          {listTypesPick && listTypesPick.map((type, index) => {
            return (
              <CategoryButton
                key={index} data={type}
                onPress={handleTypesPick} 
                isPick={true} />
            )
          })}
        </View>
        {filterTypeVisible ? (
        <View style={[style.filterWidget]}>
          {listTypesOption && listTypesOption.map((type, index) => {
            return (
              <CategoryButton key={index} data={type} 
                onPress={handleTypesOption} />
            )
          })}
        </View>
        ) : (<></>)}
        <View style={[style.filterWidget]}>
          <Pressable
            onPress={toggleCategoryFilter}
            >
            <View style={[style.filterButton, style.FillGreen]}>
              <Ionicons 
                name={filterCategoryVisible ? "md-caret-down" : "md-caret-forward"}
                size={18}
                color={Color.colorWhite}
                />
              <Text style={[style.filterText, style.TextWhite]}>Category</Text>
            </View>
          </Pressable>
          {listCategoriesPick && listCategoriesPick.map((category, index) => {
            return (
              <CategoryButton 
                key={index} data={category}
                onPress={handleCategoriesPick}
                isPick={true}
                />
            )
          })}
        </View>
        {filterCategoryVisible ? (
        <View style={[style.filterWidget]}>
          {listCategoriesOption && listCategoriesOption.map((category, index) => {
            return (
              <CategoryButton 
                key={index} data={category}
                onPress={handleCategoriesOption}
                />
            )
          })}
        </View>
        ) : (<></>)}
      </View>
      ) : (<></>)}
    </View>
  )
}

function CategoryButton (props) {
  let category = props.data;
  let isPick = props.isPick;

  return (
    <Pressable
      onPress={() => props.onPress(category)}>
      <View 
        style={[style.filterButton, category.isincome === 1 ? style.FillBlue : style.FillRed]}>
        <Text 
          style={[style.filterText, style.TextWhite]}>{isPick ? "- " : "+ "}{category.name}</Text>
      </View>
    </Pressable>
  )
}

const style = new StyleSheet.create({
  filterContainer: {
    paddingHorizontal: 5,
    paddingVertical: 5,
    backgroundColor: Color.backgroundColor
  },
  filterWidget: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap'
  },
  filterWidgetInside: {
    paddingHorizontal: 10
  },
  filterButton: {
    flexDirection: 'row',
    backgroundColor: Color.baseColor,
    paddingHorizontal: 5,
    paddingVertical: 2,
    margin: 2,
    minWidth: 50
  },
  filterText: {

  },
  TextWhite: {
    color: Color.colorWhite
  },
  FillGreen: {
    backgroundColor: Color.colorGreen
  },
  FillBlue: {
    backgroundColor: Color.colorBlue
  },
  FillRed: {
    backgroundColor: Color.colorRed
  }
})
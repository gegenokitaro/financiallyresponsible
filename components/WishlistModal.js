import { Modal, Text, TouchableOpacity, View } from "react-native";

export default function WishlistModal (props) {
  let visible = props.visible;

  const handleCancelWishlist = () => {
    props.hideModal();
  }

  return (
    <Modal
      animationType="slide"
      visible={visible}>
      <TouchableOpacity
        onPress={handleCancelWishlist}>
        <View>
          <Text>Close</Text>
        </View>
      </TouchableOpacity>
    </Modal>
  )
}
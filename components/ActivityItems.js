import { FlatList, SafeAreaView, StyleSheet, Text, View } from "react-native";
import ActivityItem from "./ActivityItem";
import Color from "../styles/Color";

export default function ActivityItems (props) {
  let data = props.data;
  
  return (
    <SafeAreaView 
      style={[styles.container]}>
      <FlatList
        overScrollMode="never"
        data={data}
        renderItem={({item}) => 
          <View 
            style={[styles.activityHistoryContainer]}>
            <View style={[styles.headerContainer]}>
              <Text 
                style={[styles.headerText]}>{item.header}</Text>
              <Text 
                style={[styles.headerText]}>{item.total.toLocaleString('id-ID')}</Text>
            </View>
            {item.data.map((list, index) => 
              <ActivityItem 
                key={index} item={list}
                activityItemHandler={props.activityItemHandler}
                />)}
          </View>
        }/>
    </SafeAreaView>
  )
}

const styles = new StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor
  },
  activityHistoryContainer: {
    paddingHorizontal: 5,
    paddingVertical: 7
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 1
  },
  headerText: {
    paddingHorizontal: 0,
    fontSize: 13,
    fontWeight: 'bold'
  }
})
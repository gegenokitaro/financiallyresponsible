// need to move modal to its own file, for easier nitpicking
// and to make App.js less clustered

import { useEffect, useState } from "react";
import { Modal, View, StyleSheet, TouchableOpacity, Pressable, Text, TextInput, Keyboard, Alert, Dimensions } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Ionicons from '@expo/vector-icons/Ionicons';
import SwitchSelector from 'react-native-switch-selector';
import CurrencyInput from 'react-native-currency-input';
import OptionPicker from "./OptionPicker";
import Color from "../styles/Color";
import Create from "../database/Create";
import Delete from "../database/Delete";

const backgroundColor = Color.colorWhite;
const borderColor = Color.colorGrey;
const buttonSubmitColor = Color.colorGreen;
const buttonUpdateColor = Color.colorBlue;
const buttonActiveColor = Color.colorBlue;
const buttonCancelColor = Color.colorRed;
const buttonTextColor = Color.colorWhite;
const textPlaceholder = Color.colorGrey;

const baseIncomeColor = Color.colorGrey;
const baseExpenseColor = Color.colorGrey;

export default function ActivityModal(props) {
  let visible = props.visible
  const db = props.db

  const [activityID, onChangeActivityID] = useState();
  const [activityIsIncome, onChangeActivityIsIncome] = useState(false);
  const [activityCategory, onChangeActivityCategory] = useState();
  const [activityPrice, onChangeActivityPrice] = useState();
  const [activityDate, onChangeActivityDate] = useState();
  const [activityDescription, onChangeActivityDescription] = useState("");

  const [activityCategories, setActivityCategories] = useState([]);
  const [activityCategoriesList, setActivityCategoriesList] = useState([]);
  const [addActivityCategory, setAddActivityCategory] = useState("");

  const [datePickerOpen, setDatePickerOpen] = useState(false);
  const [dateDefaultSet, setDateDefaultSet] = useState(false);
  const [categoryDefaultSet, setCategoryDefaultSet] = useState(false);
  const [categoryPickerOpen, setCategoryPickerOpen] = useState(false);

  const [checkInputWarning, setCheckInputWarning] = useState(false);

  useEffect(() => {
    ListCategories();
    if (props.default && props.default.id) {
      onChangeActivityID(props.default.id)
      onChangeActivityIsIncome(props.default.isincome == 1 ? true : false)
      onChangeActivityCategory(props.default.isincome)
      onChangeActivityPrice(props.default.price)
      onChangeActivityDate(new Date(props.default.date))
      onChangeActivityDescription(props.default.description)
      let category = activityCategories.filter(e => e.id == props.default.category_id)[0];
      onChangeActivityCategory(category);
    }
  }, [visible]);

  useEffect(() => {
    if (activityDate) setDateDefaultSet(true);
  }, [activityDate])

  useEffect(() => {
    if (activityCategory) setCategoryDefaultSet(true);
  }, [activityCategory])

  useEffect(() => {
    DistributeCategories(activityIsIncome ? 1 : 0);
  }, [activityIsIncome])

  useEffect(() => {
    DistributeCategories(activityIsIncome ? 1 : 0);
  }, [activityCategories])

  const DistributeCategories = (isincome) => {
    setActivityCategoriesList(activityCategories.filter(e => e.isincome == isincome))
  }

  const ListCategories = () => {
    let sql = `SELECT * FROM categories;`
    db.transaction(tx => {
      tx.executeSql(sql, [], (_, { rows }) => {
        let data = rows._array;
        setActivityCategories(data);
      })
    })
  }

  const CheckCategory = (id) => {
    return new Promise((resolve, reject) => {
      try {
        let sql = `SELECT count(*) as count FROM items WHERE category_id = ?`;
        db.transaction(tx => {
          tx.executeSql(sql, [id], (_, { rows }) => {
            let count = rows._array[0].count;
            resolve(count)
          })
        })
      } catch (error) {
        console.log(error)
        reject(error)
      }
    })
  }

  const AddCategory = async (value) => {
    let category = {};
    if (value.name) category.name = value.name;
    if (value.isincome) category.isincome = value.isincome;
    await Create(db, "categories", category)
    ListCategories(activityIsIncome);
  }

  const DeleteCategory = async (id) => {
    let countCategory = await CheckCategory(id);
    if (countCategory > 0) return;
    await Delete(db, "categories", id);
    ListCategories(activityIsIncome);
  }

  const setDefault = () => {
    onChangeActivityID();
    onChangeActivityIsIncome(false);
    ListCategories(activityIsIncome);
    onChangeActivityCategory();
    onChangeActivityPrice();
    onChangeActivityDate();
    onChangeActivityDescription();

    setDateDefaultSet(false);
    setCategoryDefaultSet(false);
    setCheckInputWarning(false);
    hideCategoryPicker();
  }

  const handleDatePicker = (date) => {
    hideDatePicker();
    onChangeActivityDate(date);
  }

  const hideDatePicker = () => {
    setDatePickerOpen(false);
  }

  const toggleCategoryPicker = () => {
    if (categoryPickerOpen) return hideCategoryPicker();
    showCategoryPicker();
  }

  const showCategoryPicker = () => {
    setCategoryPickerOpen(true);
  }

  const hideCategoryPicker = () => {
    setCategoryPickerOpen(false);
  }

  const handleSelectOption = (data) => {
    // console.log(data);
    onChangeActivityCategory(data);
    hideCategoryPicker();
  }

  const formatDate = (date) => {
    let dateYear = date.getFullYear();
    let dateMonth = (date.getMonth()+1).toString().padStart(2, '0');
    let dateDay = date.getDate().toString().padStart(2, '0');
    return `${dateYear} / ${dateMonth} / ${dateDay}`
  }

  const checkEmptyInput = () => {
    var msg = "";
    msg += "Lengkapi data terlebih dahulu.";
    msg += "\nData yang harus diisi adalah : ";
    msg += "\n- Kategori";
    msg += "\n- Tanggal";
    msg += "\n- Nominal";
    if (!activityPrice || !dateDefaultSet || !categoryDefaultSet) { 
      setCheckInputWarning(true); 
      Alert.alert('Data belum lengkap', msg)
      return false 
    }

    return true
  }

  const hideModal = () => {
    props.hideModal();
  }

  const handleAddActivity = () => {
    if (!checkEmptyInput()) return
    let item = {
      isIncome: activityIsIncome,
      category: activityCategory,
      date: activityDate,
      price: activityPrice,
      description: activityDescription,
    }
    setDefault();
    Keyboard.dismiss();
    props.onConfirm(item);
  }

  const handleUpdateActivity = () => {
    let updateItem = {};
    if (activityIsIncome != props.default.isincome) updateItem.isincome = activityIsIncome;
    if (activityCategory.id != props.default.category_id) updateItem.category_id = activityCategory.id;
    if (activityDate.toISOString() != props.default.date) updateItem.date = activityDate.toISOString();
    if (activityPrice != props.default.price) updateItem.price = activityPrice;
    if (activityDescription != props.default.description) updateItem.description = activityDescription;
    
    if (Object.keys(updateItem).length > 0) updateItem.id = props.default.id;
    
    if (!updateItem.id) return Alert.alert('Update', 'Tidak ada data yang diubah')
    props.onUpdate(updateItem);
    setDefault();
  }

  const handleCancelActivity = () => {
    hideModal();
    setDefault();
  }

  const handleDeleteActivity = () => {
    props.onDelete({id: activityID})
    setDefault();
  }

  const handleIsIncomeChange = (value) => {
    if (activityIsIncome != value) {
      setCategoryDefaultSet(false);
      onChangeActivityCategory(null);
    }
    onChangeActivityIsIncome(value);
  }

  const handleAddCategory = () => {
    if (!addActivityCategory || addActivityCategory == '') return;
    let item = {
      isincome: activityIsIncome,
      name: addActivityCategory
    }
    AddCategory(item);
    setAddActivityCategory();
  }

  const handleDeleteCategory = (value) => {
    if (!value.id) return;
    DeleteCategory(value.id)
  }

  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={visible}
      style={styles.addActivityBase}>
      <View 
        style={styles.addActivityModal}
        backgroundColor={activityIsIncome ? baseIncomeColor : baseExpenseColor}
        >
        <KeyboardAwareScrollView>
          <View style={[styles.modalTitleContainer]}>
            <Text style={styles.modalTitle}>Aktivitas</Text>
            {activityID ? (
            <TouchableOpacity
              onPress={() => handleDeleteActivity()}>
              <View style={[styles.modalButton, styles.modalButtonCancel, styles.modalButtonDelete]}>
                <Ionicons 
                  name="md-trash-bin-outline"
                  size={20}
                  color={Color.colorWhite}
                  />
              </View>
            </TouchableOpacity>
            ) : (<></>)}
          </View>
          <View
            behavior={Platform.OS === 'android' ? 'height' : 'padding'}>
            <SwitchSelector
              initial={activityIsIncome ? 1 : 0} 
              options={[
                { label: 'Pengeluaran', value: false },
                { label: 'Pemasukan', value: true },
              ]}
              onPress={handleIsIncomeChange}
              borderRadius={0}
              borderColor={activityIsIncome ? baseIncomeColor : baseExpenseColor}
              hasPadding
              height={50}
              valuePadding={5}
              buttonColor={activityIsIncome ? baseIncomeColor : baseExpenseColor}
              animationDuration={100}
              style={styles.modalItems}
              textContainerStyle={styles.switchTextContainer}
              />
            <View style={styles.modalItems}>
              <Pressable
                style={[styles.input, styles.inputPressable]}
                onPress={toggleCategoryPicker}
                >
                <Text 
                  style={[categoryDefaultSet ? '' : styles.inputPlaceholder]}
                  >{categoryDefaultSet ? activityCategory.name : 'Kategori'}</Text>
              </Pressable>
              {categoryPickerOpen ? (
                <View style={styles.selectPickerContainer}>
                  <OptionPicker 
                    options={activityCategoriesList}
                    onPress={handleSelectOption}
                    deletable={true}
                    onDelete={handleDeleteCategory}
                  />
                  <View style={styles.selectPickerAdd}>
                    <TextInput 
                      style={styles.selectPickerInput}
                      placeholder="Tambah kategori..."
                      value={addActivityCategory}
                      onChangeText={setAddActivityCategory}
                      />
                    <TouchableOpacity 
                      style={styles.selectPickerSubmit}
                      onPress={handleAddCategory}
                      >
                      <Ionicons 
                        name="md-add"
                        size={20}
                        color={Color.colorWhite}
                        />
                    </TouchableOpacity>
                  </View>
                </View>
              ) : (<></>)}
            </View>
            <View style={styles.modalItems}>
              <Pressable
                style={[styles.input, styles.inputPressable]}
                onPress={() => setDatePickerOpen(true)}
                >
                <Text 
                  style={dateDefaultSet ? styles.inputText : styles.inputPlaceholder}
                  >{dateDefaultSet ? formatDate(activityDate) : 'Tanggal'}</Text>
              </Pressable>
              <DateTimePickerModal 
                isVisible={datePickerOpen}
                date={activityDate}
                onConfirm={handleDatePicker}
                onCancel={hideDatePicker}
                />
            </View>
            <View style={styles.modalItems}>
              <CurrencyInput
                style={styles.input}
                value={activityPrice} 
                onChangeValue={onChangeActivityPrice}
                onChangeText={(formattedText) => {
                  
                }}
                delimiter='.'
                separator=','
                unit='Rp. '
                precision={0}
                placeholder='Nominal'
                placeholderTextColor={Color.colorGrey}
                />
            </View>
            <View style={styles.modalItems}>
              <TextInput 
                style={[styles.input, {textAlignVertical: 'top', paddingVertical: 7}]} 
                placeholder='Deskripsi' 
                value={activityDescription} 
                onChangeText={onChangeActivityDescription}
                placeholderTextColor={Color.colorGrey}
                editable
                multiline
                numberOfLines={3}
                />
            </View>
          </View>
        </KeyboardAwareScrollView>
        <View style={styles.modalButtonWrapper}>
          <View style={styles.modalButtonContainer}>
            <TouchableOpacity
              style={[styles.modalButton, styles.modalButtonCancel]}
              title='Batal'
              onPress={() => handleCancelActivity()}>
                <Ionicons 
                  name='md-close'
                  size={32}
                  color={Color.colorWhite}
                  />
              </TouchableOpacity>
          </View>
          {activityID ? (
          <View style={styles.modalButtonContainer}>
            <TouchableOpacity
              style={[styles.modalButton, styles.modalButtonUpdate]}
              title='Perbarui'
              onPress={() => handleUpdateActivity()}>
              <Ionicons 
                name='md-checkmark-done'
                size={32}
                color={Color.colorWhite}
                />
            </TouchableOpacity>
          </View>
          ) : (
          <View style={styles.modalButtonContainer}>
            <TouchableOpacity
              style={[styles.modalButton, styles.modalButtonSubmit]}
              title='Tambah'
              onPress={() => handleAddActivity()}>
              <Ionicons 
                name='md-checkmark'
                size={32}
                color={Color.colorWhite}
                />
              </TouchableOpacity>
          </View>
          )}
        </View>
      </View>
    </Modal>
  )
}
const styles = StyleSheet.create({
  addActivityBase: {
    flex: 0,
  },
  addActivityModal: {
    marginTop: 0,
    paddingTop: 20,
    paddingHorizontal: 30,
    // borderTopLeftRadius: 20,
    // borderTopRightRadius: 20,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  modalTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  modalTitle: {
    fontSize: 20,
    marginTop: 10,
    marginBottom: 20,
    color: Color.colorWhite
  },
  modalItems: {
    marginBottom: 20,
  },
  switchTextContainer: {
    height: '100%'
  },
  selectPickerContainer: {
    
  },
  selectPickerAdd: {
    backgroundColor: Color.colorWhite,
    flexDirection: 'row'
  },
  selectPickerInput: {
    paddingHorizontal: 15,
    paddingVertical: 1
  },
  selectPickerSubmit: {
    position: 'absolute',
    right: 0,
    padding: 4,
    backgroundColor: Color.colorBlue
  },
  selectPickerOption: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderColor: Color.colorGrey
  },
  modalButtonWrapper: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'flex-end',
    marginHorizontal: -30,
    backgroundColor: Color.colorBlue
    // position: 'absolute',
    // bottom: 0,
    // right: 20
  },
  modalButtonContainer: {
    flex: 1
  },
  modalButton: {
    height: 60,
    // width: 60,
    // borderRadius: 60,
    // marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonSubmit: {
    backgroundColor: buttonSubmitColor
  },
  modalButtonUpdate: {
    backgroundColor: buttonUpdateColor
  },
  modalButtonCancel: {
    backgroundColor: buttonCancelColor
  },
  modalButtonDelete: {
    width: 50,
    height: 50,
    borderRadius: 10
  },
  addActivityWrapper: {
    position: 'absolute',
    bottom: 25,
    right: 25,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  addWrapper: {
    height: 60,
    width: 60,
    borderRadius: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: borderColor,
    backgroundColor: buttonActiveColor
  },
  buttonText: {
    color: buttonTextColor,
  },
  input: {
    paddingVertical: 2,
    paddingHorizontal: 15,
    // borderColor: borderColor,
    // borderWidth: 1,
    backgroundColor: Color.colorWhite,
  },
  inputPressable: {
    paddingVertical: 7
  },
  inputText: {

  },
  inputPlaceholder: {
    color: textPlaceholder
  }
});
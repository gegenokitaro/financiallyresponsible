import { FlatList, SafeAreaView, StyleSheet, Text, View } from "react-native";
import Color from "../styles/Color";
import { useEffect, useState } from "react";
import ArrayMath from "../utils/ArrayMath";

export default function AnalyticItem (props) {
  let db = props.db;
  let mode = props.mode;

  const [isLoading, setIsLoading] = useState(false);

  const [data, setData] = useState([]);
  const [incomeData, setIncomeData] = useState([]);
  const [expenseData, setExpenseData] = useState([]);

  useEffect(() => {
    if (data.length == 0) groupData();
  }, [mode])

  const groupData = async () => {
    setIsLoading(true);
    let get_data = (await getData()).data;
    setData(get_data);
    let group_data = get_data.reduce((acc, value) => {
      let property = value.isincome ? 'income' : 'expense';
      acc[property] = acc[property] ? [...acc[property], value] : [value];
      return acc
    }, {})

    Object.keys(group_data).forEach(e => {
      let total = ArrayMath.sum(group_data[e], 'price');
      group_data[e].push({ id: 9999999, name: 'Total', price: total, summary: true })
    })

    setIncomeData(group_data['income'] ? group_data['income'] : []);
    setExpenseData(group_data['expense'] ? group_data['expense'] : []);
    setIsLoading(false);
  }

  const getData = () => {
    return new Promise((resolve, reject) => {
      try {
        let sql = `
        SELECT
          categories.id
          , categories.name
          , items.isincome as isincome
          , sum(items.price) as price
        FROM items
        JOIN categories 
          ON categories.id = items.category_id
        WHERE 1=1
        GROUP BY categories.id, categories.name, items.isincome
        ORDER by isincome DESC, price DESC;`;
        let input = [];
        db.transaction(tx => {
          tx.executeSql(sql, input, (_, result) => {
            let data = result.rows._array;
            resolve({data});
          })
        })
      } catch (error) {
        console.log(error);
        reject({error});
      }
    })
  }

  return (
    <SafeAreaView style={[styles.container]}>
      {!isLoading ? (<>
        {incomeData.length > 0 ? 
          <Items 
            title={'Pemasukan'} 
            data={incomeData} /> : <></>}
        {expenseData.length > 0 ? 
          <Items 
            title={'Pengeluaran'} 
            data={expenseData} /> : <></>}
        </>) : (<></>)}
    </SafeAreaView>
  )
}

function Items (props) {
  let title = props.title;
  let data = props.data;
  return (
    <View style={[styles.itemWrapper]}>
      <Text style={[styles.TextBold]}>{title}</Text>
      <FlatList
        overScrollMode="never"
        data={data}
        renderItem={({item}) => 
          <View style={[styles.itemContainer, item.summary ? null : styles.PaddingLeft]}>
            <Text style={[item.summary ? styles.TextBold : null]}>{item.name}</Text>
            <Text style={[item.summary ? styles.TextBold : null]}>{item.price.toLocaleString('id-ID')}</Text>
          </View>
        }/>
      </View>
  )
}

const styles = new StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor
  },
  itemWrapper: {
    paddingVertical: 2,
    paddingHorizontal: 10
  },
  itemContainer: {
    paddingVertical: 2,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between'
  },
  TextBold: {
    fontWeight: 'bold'
  },
  PaddingLeft: {
    marginLeft: 10
  }
})
import { View, Text, StyleSheet } from "react-native";

export default function ActivityHeader(props) {
  let saldo = props.saldo;
  let income = props.income;
  let expense = props.expense;

  return (
    <View style={styles.headerContainer}>
      <View style={[styles.headerItem, styles.BackgroundGreen]}>
        <Text style={[styles.headerText, styles.TextLight]}>Saldo</Text>
        <Text style={[styles.headerText, styles.TextLight]}>{saldo.toLocaleString('id-ID')}</Text>
      </View>
      <View style={[styles.headerItem, styles.BackgroundGreen]}>
        <Text style={[styles.headerText, styles.TextLight]}>Pemasukan</Text>
        <Text style={[styles.headerText, styles.TextLight]}>{income.toLocaleString('id-ID')}</Text>
      </View>
      <View style={[styles.headerItem, styles.BackgroundGreen]}>
        <Text style={[styles.headerText, styles.TextLight]}>Pengeluaran</Text>
        <Text style={[styles.headerText, styles.TextLight]}>{expense.toLocaleString('id-ID')}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-start',
    paddingHorizontal: 0,
    paddingVertical: 0
  },
  headerItem: {
    width: '33.33%',
    backgroundColor: "#eee",
    paddingHorizontal: 5,
    paddingVertical: 5
  },
  headerText: {
    fontSize: 13
  },
  BackgroundGreen: {
    backgroundColor: "#06d6a0"
  },
  BackgroundBlue: {
    backgroundColor: "#118ab2"
  },
  BackgroundRed: {
    backgroundColor: "#ef476f"
  },
  TextLight: {
    color: "#f9f7f3"
  }
})
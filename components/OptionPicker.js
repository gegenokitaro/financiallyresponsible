import { TouchableOpacity, View, Text, StyleSheet } from "react-native"
import Ionicons from '@expo/vector-icons/Ionicons';
import Color from "../styles/Color"

export default function OptionPicker(props) {
  const options = props.options
  const deletable = props.deletable

  return (
    <View>
      {options.map(option => {
        return (
          <View 
            key={option.id}
            style={styles.optionWrapper}
            >
            <TouchableOpacity 
              style={styles.optionPressable}
              onPress={() => props.onPress(option)}
              >
              <View style={styles.optionCategory}>
                <Text>{option.name}</Text>
              </View>
            </TouchableOpacity>
            {deletable ? (
              <View style={styles.categoryDelete}>
                <TouchableOpacity 
                  style=""
                  onPress={() => props.onDelete(option)}
                  >
                  <Ionicons 
                    name="md-close"
                    size={20}
                    color={Color.colorWhite}
                    />
                </TouchableOpacity>
              </View>
            ) : (<></>)}
          </View>
        )
      })}
    </View>
  )
}

const styles = new StyleSheet.create({
  optionWrapper: {
    backgroundColor: Color.colorWhite,
    flexDirection: 'row',
    alignItems: 'flex-start',
    flex: 1
  },
  optionPressable: {
    width: '100%'
  },
  optionCategory: {
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  categoryDelete: {
    position: 'absolute',
    right: 0,
    padding: 4,
    backgroundColor: Color.colorRed
  }
})